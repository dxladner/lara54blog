<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@index');

// test route 
Route::get('test', 'TestController@index');

Auth::routes();

Route::get('widget/create',  'WidgetController@create')->name('widget.create');
Route::get('widget/{widget}-{slug?}', 'WidgetController@show')->name('widget.show');
Route::resource('widget', 'WidgetController', ['except' => ['show', 'create']]);

Route::get('/admin', 'AdminController@index')->name('admin');

Route::get('terms-of-service', 'PagesController@terms');
Route::get('privacy', 'PagesController@privacy');

Route::resource('profile', 'ProfileController');
Route::get('show-profile','ProfileController@showProfileToUser')->name('show-profile');
Route::get('determine-profile-route','ProfileController@determineProfileRoute')->name('determine-profile-route');

